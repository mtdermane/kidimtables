import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import '0_loginWidgets/loginPage.dart';
import '1_menuLayout.dart';
import 'package:shared_preferences/shared_preferences.dart';


class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {

  bool _ready = false;
  String ? _placeId;
  String ? _table;

  Future<void> getLocalInformation() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String ? placeId = prefs.getString('placeId');
    String ? table = prefs.getString('table');
    setState(() {
      _ready = true;
    });
    if(placeId != null && table != null){
      setState(() {
        _placeId = placeId;
        _table = table;
      });
    }
  }

  Widget launchLayout(){
    if(_ready){
      if(_placeId != null && _table != null && _placeId != "" && _table != ""){
        return TypeListPage(_placeId, _table);
      }
      else{
        return LoginLayout();
      }
    }
    return SpinKitPouringHourGlassRefined(
      color: Colors.red.shade600,
      size: 250.0,
    );
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getLocalInformation();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.orange[50],
        body: launchLayout(),
    );
  }
}
