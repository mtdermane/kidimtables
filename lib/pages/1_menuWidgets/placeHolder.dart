import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';


///placeHolder during the loading
class TypePlaceHolder extends StatelessWidget {
  final double _leftIconSize = 50.0;
  final double _leftIconMargin = 15.0;
  final double _verticalMargin = 2.5;
  final double _horizontalMargin = 20.0;
  final double _containerHeight = 60.0;

  @override
  Widget build(BuildContext context) {
    double screenSize = MediaQuery.of(context).size.width;
    double maxTextHolderSize = screenSize -
        2 * _leftIconSize -
        _leftIconMargin -
        2 * _horizontalMargin;
    return Container(
      child: SizedBox(
        width: 200.0,
        height: 100.0,
        child: Shimmer.fromColors(
          baseColor: Colors.orange.shade200,
          highlightColor: Colors.red.shade300,
          child: Container(
            height: _containerHeight,
            margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 5.0),
            decoration:
            BoxDecoration(borderRadius: BorderRadius.circular(50.0)),
            child: Row(
              children: <Widget>[
                Container(
                  width: _leftIconSize,
                  height: _leftIconSize,
                  margin: EdgeInsets.only(right: _leftIconMargin),
                  color: Colors.brown,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      width: maxTextHolderSize / 3,
                      height: 10.0,
                      margin: EdgeInsets.symmetric(
                          vertical: _verticalMargin,
                          horizontal: _horizontalMargin),
                      color: Colors.brown,
                    ),
                    Container(
                      width: maxTextHolderSize * 2 / 3,
                      height: 10.0,
                      margin: EdgeInsets.symmetric(
                          vertical: _verticalMargin,
                          horizontal: _horizontalMargin),
                      color: Colors.brown,
                    ),
                    Container(
                      width: maxTextHolderSize,
                      height: 10.0,
                      margin: EdgeInsets.symmetric(
                          vertical: _verticalMargin,
                          horizontal: _horizontalMargin),
                      color: Colors.brown,
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
