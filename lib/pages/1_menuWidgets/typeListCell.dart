import 'package:flutter/material.dart';
import '../../models/menuItem.dart';
import 'package:flutter_svg/flutter_svg.dart';
import '../../widgets/globalVariables.dart' as global;
import '../../models/cartContent.dart' as cartItemContent;
import '../../translations.dart';
import 'package:sticky_headers/sticky_headers.dart';
import 'package:Kidim/models/option.dart';

/// Class containing the item type template
class TypeItem extends StatefulWidget {
  final MenuList _menuList;
  final Function _updateCart;
  final double _typeFontSize = 25;

  TypeItem(this._menuList, this._updateCart);

  @override
  _TypeItemState createState() => _TypeItemState();
}

class _TypeItemState extends State<TypeItem> {

  Widget headerList() {
    return Container(
      color: Colors.orange[50],
      child: ListTile(
        title: Text(
          widget._menuList.category ?? "",
          textAlign: TextAlign.center,
          style: TextStyle(
            color: Colors.brown[600],
            fontWeight: FontWeight.bold,
            fontFamily: 'Baskerville',
            fontSize: widget._typeFontSize,
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return StickyHeader(
      header: headerList(),
      content: ListView.builder(
        physics: new NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
        itemCount: widget._menuList.items?.length,
        itemBuilder: (BuildContext context, int index) {
          return CellContent(widget._updateCart, widget._menuList.items[index]);
        },
      ),
    );
  }
}

class CellContent extends StatefulWidget {
  final Function _updateCart;
  final MenuItem _item;
  CellContent(this._updateCart, this._item);

  @override
  _CellContentState createState() => _CellContentState();
}

class _CellContentState extends State<CellContent> {

  final double _mealFontSize = 20.0;

  final double _mealSubtitleFontSize = 15.0;

  final double _leadingWidgetWidth = 50.0;

  String ? optionValue;
  String ? optionName;
  late bool isExpanded;

  @override
  void initState() {
    super.initState();
    optionValue = null;
    optionName = null;
    isExpanded = false;
    if(widget._item.options.length > 0){
      optionName = widget._item?.options[0]["name"];
      optionValue = widget._item?.options[0]["value"];
    }
  }

  Widget expansionChildren(MenuItem item) {
    if (item.options.length > 0) {
      return ListView.builder(
        physics: new NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
        itemCount: item.options.length,
        itemBuilder: (BuildContext context, int index) {
          return SizedBox(
            height: 40,
            child: ListTile(
              contentPadding: EdgeInsets.symmetric(vertical: 0.0, horizontal: 16),
              leading: SizedBox(
                height: 24.0,
                width: 24.0,
                child: Radio(
                  activeColor: Colors.brown,
                  value: item.options[index]['name'],
                  groupValue: optionName,
                  onChanged: (value) {
                    setState(() {
                      optionValue = item.options[index]['price'];
                      optionName = value;
                    });
                  },
                ),
              ),
              title: Text(item.options[index]['name']),
              trailing:  item.options[index]['price'] != '0' ? Text('\$${item.options[index]['price']}') : null,
              onTap: () {
                setState(() {
                  optionValue = item.options[index]['price'];
                  optionName = item.options[index]['name'];
                });
              },
            ),
          );
        },
      );
    }
    return SizedBox.shrink();
  }

  Widget mealWithOptions(BuildContext context, MenuItem item, Widget svgType){
    return ExpansionTile(
      onExpansionChanged: (value){
        setState(() {
          isExpanded = value;
        });
      },
      leading: Container(
        width: _leadingWidgetWidth,
        child: svgType,
      ),
      title: Text(
        item.name ?? "",
        style: TextStyle(
          fontFamily: 'Baskerville',
          fontSize: _mealFontSize,
        ),
      ),
      subtitle: item.description != "" && item.description != null
          ? Text(
              item.description,
              style: TextStyle(
                fontFamily: 'Baskerville',
                fontSize: _mealSubtitleFontSize,
              ),
            )
          : null,
      trailing: Container(
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text(
              (item?.price != null && item?.price != '0')
                  ? "\$ ${item?.price}"
                  : "",
              style: TextStyle(
                  fontFamily: 'Baskerville',
                  fontSize: _mealFontSize,
                  color: Colors.brown[600]),
            ),
            IconButton(
              icon: Icon(
                isExpanded ? Icons.add_circle_outline : Icons.expand_more,
                color: Colors.red,
                size: 35.0,
              ),
              onPressed: !isExpanded ? null : () {
                global.cart.add(
                  new cartItemContent.CartContent(
                    name: item.name,
                    price: item.price,
                    option: new Option(optionName!, optionValue!),
                    addons: [],
                  ),
                );
                widget._updateCart();
                final snackBar = SnackBar(
                  content: Text(
                    item.name + Translations.of(context)!.text('addedToCart'),
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontFamily: 'Baskerville',
                        fontSize: _mealFontSize,
                        color: Colors.white),
                  ),
                  backgroundColor: Colors.orange[600],
                  duration: Duration(milliseconds: 500),
                );

                // Find the Scaffold in the widget tree and use
                // it to show a SnackBar.
                ScaffoldMessenger.of(context).showSnackBar(snackBar);
              },
            ),
          ],
        ),
      ),
      children: <Widget>[
        expansionChildren(item),
      ],
    );
  }

  Widget mealWithoutOptions(BuildContext context, MenuItem item, Widget svgType){
    return ListTile(
      leading: Container(
        width: _leadingWidgetWidth,
        child: svgType,
      ),
      title: Text(
        item.name ?? "",
        style: TextStyle(
          fontFamily: 'Baskerville',
          fontSize: _mealFontSize,
        ),
      ),
      subtitle: item.description != "" && item.description != null
          ? Text(
              item.description ?? "",
              style: TextStyle(
                fontFamily: 'Baskerville',
                fontSize: _mealSubtitleFontSize,
              ),
            )
          : null,
      trailing: Container(
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text(
              (item?.price != null && item?.price != '0')
                  ? "\$ ${item?.price}"
                  : "",
              style: TextStyle(
                  fontFamily: 'Baskerville',
                  fontSize: _mealFontSize,
                  color: Colors.brown[600]),
            ),
            IconButton(
              icon: Icon(
                Icons.add_circle_outline,
                color: Colors.red,
                size: 35.0,
              ),
              onPressed: () {
                global.cart.add(
                  new cartItemContent.CartContent(
                    name: item.name,
                    price: item.price,
                    option: null,
                    addons: [],
                  ),
                );
                widget._updateCart();
                final snackBar = SnackBar(
                  content: Text(
                    item.name + Translations.of(context)!.text('addedToCart'),
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontFamily: 'Baskerville',
                        fontSize: _mealFontSize,
                        color: Colors.white),
                  ),
                  backgroundColor: Colors.orange[600],
                  duration: Duration(milliseconds: 500),
                );

                // Find the Scaffold in the widget tree and use
                // it to show a SnackBar.
                ScaffoldMessenger.of(context).showSnackBar(snackBar);
              },
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final String _assetName = 'images/icon_type.svg';
    final Widget svgType =
        new SvgPicture.asset(_assetName, semanticsLabel: 'icon type');
    return widget._item.options.length > 0 ? mealWithOptions(context, widget._item, svgType) : mealWithoutOptions(context, widget._item, svgType);
  }
}
