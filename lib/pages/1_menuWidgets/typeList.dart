import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import '../../models/menuItem.dart';
import "package:collection/collection.dart";
import '../../widgets/globalVariables.dart' as global;
import 'package:shared_preferences/shared_preferences.dart';
import 'placeHolder.dart';
import 'typeListCell.dart';
///Content widget

class TypeListWidget extends StatefulWidget {
  final String ? _placeId;
  late final Function _updateCart;
  final int _placeHolderItemNumber = 3;
  TypeListWidget(this._placeId, this._updateCart);

  @override
  _TypeListWidgetState createState() => _TypeListWidgetState();
}

class _TypeListWidgetState extends State<TypeListWidget> {
  int ? _statusCode;
  List _menu = [];

  ///Function used to create a post
  Future<void> makePostRequest(String url) async {
    final response = await get(url as Uri);

    final int statusCode = response.statusCode;

    setState(() {
      _statusCode = statusCode;
    });

    if (statusCode < 200 || statusCode > 400 || json == null) {
      throw new Exception("Error while fetching data");
    }

    global.menu = groupBy(
        jsonDecode(response.body)["data"],
            (Map obj) => obj['category']
    );

    List menu = [];
    MenuList menuList;
    List items;
    List options;
    global.menu.forEach(
      (category, mealsList) => {
        items = [],
        menuList = new MenuList(category, items),

        mealsList.forEach(
          (aMeal) => {
            options = aMeal["options"] != null ? aMeal["options"] : [],
            items.add(new MenuItem(aMeal["name"], aMeal["price"].toString(), aMeal["description"], options)),
          }
        ),

        // menuList.items = items,

        menu.add(menuList),
      }
    );

    setState(() {
      _menu = menu.toList();
    });
  }

  void getMenu() async {
    //setting language and url variables
    String ? language;
    String ? serverAddress;
    String ? url;

    //getting the language
    final prefs = await SharedPreferences.getInstance();
    language = prefs.getString("language");
    if(language == null){
      prefs.setString('language', "en");
      language = "en";
    }

    //getting url
    serverAddress = await global.storage.read(key: "serverAddress");
    url = "${serverAddress}menu/?placeId=${widget._placeId}&language=$language";

    //calling the server request
    makePostRequest(url);
  }

  @override
  void initState() {
    super.initState();
    getMenu();
  }

  @override
  Widget build(BuildContext context) {
    if (_statusCode == null || _menu?.length == null) {
      return SafeArea(
        child: ListView.builder(
          padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
          itemCount: widget._placeHolderItemNumber,
          itemBuilder: (BuildContext context, int index) {
            return TypePlaceHolder();
          },
        ),
      );
    }

    return SafeArea(
      child: ListView.builder(
        padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
        itemCount: _menu?.length,
        itemBuilder: (BuildContext context, int index) {
          MenuList menuList = _menu[index];
          return Container(
            child: TypeItem(
              menuList,
              widget._updateCart
            ),
          );
        },
      ),
    );
  }
}
