import 'package:flutter/material.dart';
import 'package:badges/badges.dart' as badges;
import '../../pages/2_cartLayout.dart';
import '../../widgets/globalVariables.dart' as global;
import 'package:shared_preferences/shared_preferences.dart';
import 'typeList.dart';
import '../../translations.dart';

/// Default Layout is the layout that will be use to make alost all the pages
/// it consists in a toolbar containing a title the cart item and the body theme color

class DefaultLayout extends StatefulWidget {
  //props required to use the default layout title is the tile of toolbar
  late final String _title;
  //content is the body content
  final String ? _placeId;
  final String ? _table;
  final double ? _mealFontSize = 20.0;

  DefaultLayout(this._title, this._placeId, this._table);

  @override
  _DefaultLayoutState createState() => _DefaultLayoutState();
}

class _DefaultLayoutState extends State<DefaultLayout> {
  int _badgeValue = 0;

  void updateCart() {
    setState(() {
      _badgeValue = global.cart.length;
    });
  }

  void changeLanguage() async {
    final prefs = await SharedPreferences.getInstance();
    final currentLanguage = prefs.getString("language");

    if (currentLanguage == null) {
      prefs.setString('language', "en");
    } else {
      prefs.setString('language', currentLanguage == "en" ? "fr" : "en");
    }
  }

  Widget cartIcon() {
    if (_badgeValue > 0) {
      return badges.Badge(
        badgeContent: Text(
          _badgeValue.toString(),
          style: TextStyle(
            fontFamily: 'Baskerville',
            color: Colors.red,
          ),
        ),
        badgeStyle: badges.BadgeStyle(
          badgeColor: Colors.white,
        ),
        child: Icon(Icons.room_service),
        badgeAnimation: badges.BadgeAnimation.slide(),
      );
    }
    return Icon(Icons.room_service);
  }

  @override
  void didUpdateWidget(DefaultLayout oldWidget) {
    super.didUpdateWidget(oldWidget);
    updateCart();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.orange[50],
      appBar: AppBar(
        title: Text(
          widget._title,
          style: TextStyle(color: Colors.white),
        ),
        backgroundColor: Colors.red[800],
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.language),
            iconSize: 45.0,
            tooltip: Translations.of(context)!.text('cart'),
            onPressed: () {
              changeLanguage();
              final snackBar = SnackBar(
                content: Text(
                  Translations.of(context)!.text('changeLanguage'),
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontFamily: 'Baskerville',
                      fontSize: widget._mealFontSize,
                      color: Colors.white),
                ),
                backgroundColor: Colors.brown,
                duration: Duration(milliseconds: 2000),
              );
              ScaffoldMessenger.of(context).showSnackBar(snackBar);
            },
          ),
          IconButton(
            icon: cartIcon(),
            iconSize: 45.0,
            tooltip: Translations.of(context)!.text('cart'),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => CartPage()),
              );
            },
          )
        ],
      ),
      body: TypeListWidget(widget._placeId, updateCart),
    );
  }
}
