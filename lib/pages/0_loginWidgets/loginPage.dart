import 'dart:collection';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import '../1_menuLayout.dart';
import '../../models/posts.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../widgets/globalVariables.dart' as global;
import '../../translations.dart';

class LoginLayout extends StatefulWidget {
  final double _buttonFontSize = 25.0;
  final double _buttonWidth = 200.0;
  final double _mainContainerMargin = 80.0;
  final double _errorLoginFontSize = 20.0;

  @override
  _LoginLayoutState createState() => _LoginLayoutState();
}

class _LoginLayoutState extends State<LoginLayout> {
  TextEditingController _usernameField = new TextEditingController();
  TextEditingController _passwordField = new TextEditingController();
  TextEditingController _tableField = new TextEditingController();
  bool _isLoginFailed = false;
  late String _tableValue;
  final _formKey = GlobalKey<FormState>();

  ///Login function
  void login(String username, String password, String table) async {
    //getting url
    String ? url = await global.storage.read(key: "serverAddress");
    String extension = 'authentication';
    url = url !+ extension;

    setState(() {
      this._tableValue = table;
    });

    //setting the request post body
    PostLogin loginRequest = new PostLogin(
        username: username, password: password, strategy: 'local');

    //calling the server request
    await makePostRequest(url, body: loginRequest.toMap());
  }

  ///Function used to create a post
  Future<void> makePostRequest(String url, {required Map body}) async {
    return post(url as Uri, body: body).then((Response response) async {
      final int statusCode = response.statusCode;

      if (statusCode < 200 || statusCode > 400) {
        throw new Exception("Error while fetching data");
      }
      Map auth = new HashMap();
      auth = jsonDecode(response.body);
      //get the place id
      String placeId = auth["user"]["placeId"];
      final prefs = await SharedPreferences.getInstance();
      await prefs.setString('placeId', placeId);
      await prefs.setString('table', this._tableValue);
      setState(() {
        _isLoginFailed = true;
      });
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
            builder: (context) => TypeListPage(placeId, _tableField.text)),
      );
    });
  }

  static final _kidimImg = 'images/kidim.svg';
  final Widget _kidim = new SvgPicture.asset(
    _kidimImg,
    color: Colors.red[600],
    semanticsLabel: 'Main title',
  );

  static final _borderTR = 'images/borderTR.svg';
  final Widget _svgBorderTR = new SvgPicture.asset(
    _borderTR,
    color: Colors.brown,
    semanticsLabel: 'Main background',
  );

  static final _borderTL = 'images/borderTL.svg';
  final Widget _svgBorderTL = new SvgPicture.asset(
    _borderTL,
    color: Colors.brown,
    semanticsLabel: 'Main background',
  );

  static final _borderBR = 'images/borderBR.svg';
  final Widget _svgBorderBR = new SvgPicture.asset(
    _borderBR,
    color: Colors.brown,
    semanticsLabel: 'Main background',
  );

  static final _borderBL = 'images/borderBL.svg';
  final Widget _svgBorderBL = new SvgPicture.asset(
    _borderBL,
    color: Colors.brown,
    semanticsLabel: 'Main background',
  );

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.orange[50],
      body: Stack(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                height: 200.0,
                width: 200.0,
                child: _svgBorderTR,
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                height: 200.0,
                width: 200.0,
                child: _svgBorderTL,
              ),
            ],
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Container(
                    height: 200.0,
                    width: 200.0,
                    child: _svgBorderBR,
                  ),
                ],
              ),
            ],
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                height: 200.0,
                width: 200.0,
                child: _svgBorderBL,
              ),
            ],
          ),
          Form(
            key: _formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Center(
                  child: Container(
                    margin: EdgeInsets.symmetric(
                        vertical: 0.0, horizontal: widget._mainContainerMargin),
                    child: _kidim,
                  ),
                ),
//              _svgDecorativeLine,
                Container(
                  padding: EdgeInsets.symmetric(
                    vertical: 10.0,
                    horizontal: 100.0,
                  ),
                  child: Theme(
                    data: theme.copyWith(primaryColor: Colors.brown),
                    child: TextFormField(
                      controller: _usernameField,
                      cursorColor: Colors.brown,
                      decoration: InputDecoration(
                        icon: Icon(
                          Icons.person,
                          size: 30.0,
                        ),
                        labelText: Translations.of(context)!.text('username'),
                      ),
                      validator: (value) {
                        if (value!.isEmpty) {
                          return Translations.of(context)!.text('emptyLogin');
                        }
                        return null;
                      },
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.symmetric(
                    vertical: 10.0,
                    horizontal: 100.0,
                  ),
                  child: Theme(
                    data: theme.copyWith(primaryColor: Colors.brown),
                    child: TextFormField(
                      controller: _passwordField,
                      obscureText: true,
                      cursorColor: Colors.brown,
                      decoration: InputDecoration(
                        icon: Icon(
                          Icons.lock,
                          size: 30.0,
                        ),
                        labelText: Translations.of(context)!.text('password'),
                      ),
                      validator: (value) {
                        if (value!.isEmpty) {
                          return Translations.of(context)!.text('emptyLogin');
                        }
                        return null;
                      },
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.symmetric(
                    vertical: 10.0,
                    horizontal: 100.0,
                  ),
                  child: Theme(
                    data: theme.copyWith(primaryColor: Colors.brown),
                    child: TextFormField(
                      controller: _tableField,
                      cursorColor: Colors.brown,
                      decoration: InputDecoration(
                        icon: Icon(
                          Icons.location_on,
                          size: 30.0,
                        ),
                        labelText: Translations.of(context)!.text('table'),
                      ),
                      validator: (value) {
                        if (value!.isEmpty) {
                          return Translations.of(context)!.text('emptyLogin');
                        }
                        return null;
                      },
                    ),
                  ),
                ),
                Center(
                  child: Container(
                    margin: EdgeInsets.symmetric(
                        vertical: 0.0, horizontal: widget._mainContainerMargin),
                    width: widget._buttonWidth,
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        backgroundColor: Colors.red[600]
                      ),
                      onPressed: () {
                        if (_formKey.currentState!.validate()) {
                          login(_usernameField.text, _passwordField.text,
                              _tableField.text);
                          setState(() {
                            _usernameField = new TextEditingController();
                            _passwordField = new TextEditingController();
                            _tableField = new TextEditingController();
                          });
                          if (_isLoginFailed) {
                            setState(() {
                              _isLoginFailed = false;
                            });
                            final snackBar = SnackBar(
                              content: Text(
                                Translations.of(context)!.text('loginIncorrect'),
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontFamily: 'Baskerville',
                                    fontSize: widget._errorLoginFontSize,
                                    color: Colors.white),
                              ),
                              backgroundColor: Colors.red[600],
                              duration: Duration(seconds: 1),
                            );
                            // Find the Scaffold in the widget tree and use
                            // it to show a SnackBar.
                            ScaffoldMessenger.of(context).showSnackBar(snackBar);
                          }
                        }
                      },
                      child: Text(
                        Translations.of(context)!.text('login'),
                        style: TextStyle(
                          fontFamily: 'Baskerville',
                          color: Colors.white,
                          fontSize: widget._buttonFontSize,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
