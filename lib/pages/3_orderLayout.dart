import 'package:flutter/material.dart';
import '0_loginWidgets/loginPage.dart';
import '3_orderWidgets/sendContent.dart';
import '../widgets/defaultLayout.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import '../translations.dart';

class SendOrderPage extends StatefulWidget {
  @override
  _SendOrderPageState createState() => _SendOrderPageState();
}

class _SendOrderPageState extends State<SendOrderPage> {

  bool _ready = false;
  String ? _placeId;
  String ? _table;

  Future<void> getLocalInformation() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String ? placeId = prefs.getString('placeId');
    String ? table = prefs.getString('table');
    if(placeId != null && table != null){
      setState(() {
        _placeId = placeId;
        _table = table;
        _ready = true;
      });
    }
  }

  Widget launchLayout(){
    if(_ready){
      if(_placeId != null){
        return SendOrderContent(_table, _placeId);
      }
      else{
        return LoginLayout();
      }
    }
    return SpinKitPouringHourGlassRefined(
      color: Colors.red.shade600,
      size: 250.0,
    );
  }
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getLocalInformation();
  }

  @override
  Widget build(BuildContext context) {
    return CartLessLayout(Translations.of(context)!.text('sendOrderTitle'), launchLayout());
  }
}
