import 'package:flutter/material.dart';
import '../../models/cartContent.dart' as cartItemContent;
import '../../widgets/globalVariables.dart' as global;
import 'cartCell.dart';
import '../3_orderLayout.dart';
import '../../translations.dart';

///Class called when the cart contains at least one item
class CartContent extends StatefulWidget {
  final double _cellHeight = 100.0;
  final double _buttonWidth = 200.0;
  final double _orderFontSize = 25.0;
  final Function _updateLayout;
  CartContent(this._updateLayout);

  @override
  _CartContentState createState() => _CartContentState();
}

class _CartContentState extends State<CartContent> {
  List<cartItemContent.CartContent> _cart = global.cart;//usage of a local cart to change the state
  double _total = 0;
  ///When the cart content changes updates the layout
  updateCartItems(){
    widget._updateLayout();
    setState(() {
      _cart = global.cart;
    });
    setTotal();
  }

  ///update the value of the total when there are changes
  void setTotal(){
    double total = 0;

    for(var i = 0; i < global.cart.length; i++){
      double defaultPrice =  double.parse(global.cart[i].price);
      double optionPrice =  double.parse(global.cart[i].option?.price ?? '0');
      double result = defaultPrice + optionPrice;
      total += result;
    }

    setState(() {
      _total = total;
    });
  }

  @override
  void initState() {
    super.initState();
    setTotal();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          padding: EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 0.0),
          width: widget._buttonWidth,
          child: ElevatedButton(
            style: ElevatedButton.styleFrom(
                backgroundColor: Colors.red[600]
            ),
            child: Text(
            Translations.of(context)!.text('order'),
              textAlign: TextAlign.center,
              style: TextStyle(
                fontFamily: 'Baskerville',
                color: Colors.white,
                fontSize: widget._orderFontSize,
              ),
            ),
            onPressed: (){
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => SendOrderPage()),
              );
            },
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(18.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                "Total : ",
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontFamily: 'Baskerville',
                  fontSize: widget._orderFontSize,
                ),
              ),
              Text(
                "\$ ${_total.toString()}",
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontFamily: 'Baskerville',
                  color: Colors.brown[600],
                  fontSize: widget._orderFontSize,
                ),
              ),
            ],
          ),
        ),
        Expanded(
          child: ListView.builder(
              padding: const EdgeInsets.all(8.0),
              itemCount: _cart.length,
              itemBuilder: (BuildContext context, int index) {
                return Container(
                  height: widget._cellHeight,
                  child: CartCell(index, updateCartItems),
                );
              }),
        ),
      ],
    );
  }
}
