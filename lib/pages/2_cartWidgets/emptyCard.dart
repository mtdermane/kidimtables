import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import '../../translations.dart';

/// Class called when the cart is empty
class CartEmpty extends StatelessWidget {
  static final _emptyCartImage = 'images/icon_plat.svg';

  final Widget _emptyCart = new SvgPicture.asset(
    _emptyCartImage,
    color: Colors.grey[600],
    semanticsLabel: 'Empty Cart',
  );
  final double _emptyImageSize = 250.0;

  static final _decorativeImage = 'images/alter_decorative_line.svg';

  final Widget _decorativeLine = new SvgPicture.asset(
    _decorativeImage,
    semanticsLabel: 'Decorative line',
  );
  final double _decorativeMargin = 20.0;

  final double _emptyCartFontSize = 25.0;
  final double _emptyMessageMargin = 20.0;
  final double _buttonWidth = 200.0;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            width: _emptyImageSize,
            height: _emptyImageSize,
            child: _emptyCart,
          ),
          Container(
            margin: EdgeInsets.symmetric(
              vertical: _emptyMessageMargin,
              horizontal: _emptyMessageMargin,
            ),
            child: Text(
              Translations.of(context)!.text('emptyCardText'),
              maxLines: 5,
              textAlign: TextAlign.center,
              style: TextStyle(
                fontFamily: 'Baskerville',
                fontSize: _emptyCartFontSize,
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.symmetric(
                horizontal: _decorativeMargin, vertical: 30.0),
            child: _decorativeLine,
          ),
          Container(
            width: _buttonWidth,
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                  backgroundColor: Colors.red[600]
              ),
              child: Text(
                Translations.of(context)!.text("add"),
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontFamily: 'Baskerville',
                  color: Colors.white,
                  fontSize: _emptyCartFontSize,
                ),
              ),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
          )
        ],
      ),
    );
  }
}