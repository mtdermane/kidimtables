import 'package:flutter/material.dart';
import '../../widgets/globalVariables.dart' as global;

///Cell of the cart
class CartCell extends StatefulWidget {
  final int _index;
  final Function _updateCart;
  final double _mealFontSize = 20.0;
  final double _detailsFontSize = 15.0;

  CartCell(this._index, this._updateCart);

  @override
  _CartCellState createState() => _CartCellState();
}

class _CartCellState extends State<CartCell> {

  String getPrice(){
    double defaultPrice =  double.parse(global.cart[widget._index].price);
    double optionPrice =  double.parse(global.cart[widget._index].option?.price ?? '0');
    double result = defaultPrice + optionPrice;
    return result.toString();
  }

  ///This widget displays either a line containing subtitiles or not
  Widget displayLine(){
    bool hasNoSubtitle = ((global.cart[widget._index].option?.name ?? "") == "")
        && ((global.cart[widget._index].addons!.length ?? 0) == 0);
    if(hasNoSubtitle) {
      return Column(
        children: <Widget>[
          ListTile(
            leading: IconButton(
              icon: Icon(
                  Icons.remove_circle_outline, color: Colors.red, size: 35.0),
              onPressed: () {
                global.cart.removeAt(widget._index);
                widget._updateCart();
              },
            ),
            title: Text(
              global.cart[widget._index].name ?? "",
              textAlign: TextAlign.left,
              style: TextStyle(
                fontFamily: 'Baskerville',
                fontSize: widget._mealFontSize,
              ),
            ),
            trailing: Text(
              (global.cart[widget._index].price != null && global.cart[widget._index].price != '0')
                  ? "\$ ${global.cart[widget._index].price}"
                  : "",
              textAlign: TextAlign.left,
              style: TextStyle(
                fontFamily: 'Baskerville',
                color: Colors.brown,
                fontSize: widget._mealFontSize,
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 20.0),
            child: Divider(
              color: Colors.grey[400],
            ),
          )
        ],
      );
    }
    return  Column(
      children: <Widget>[
        ListTile(
          leading: IconButton(
            icon: Icon(Icons.remove_circle_outline, color: Colors.red, size: 35.0),
            onPressed: (){
              global.cart.removeAt(widget._index);
              widget._updateCart();
            },
          ),
          title: Text(
            global.cart[widget._index].name?? "",
            textAlign: TextAlign.left,
            style: TextStyle(
              fontFamily: 'Baskerville',
              fontSize: widget._mealFontSize,
            ),
          ),
          subtitle: Visibility(
            visible: true,
            child: Text(
              global.cart[widget._index].option?.name ?? "",
              textAlign: TextAlign.left,
              style: TextStyle(
                fontFamily: 'Baskerville',
                fontSize: widget._detailsFontSize,
              ),
            ),
          ),
          trailing: Text(
            "\$ ${getPrice()}",
            textAlign: TextAlign.left,
            style: TextStyle(
              fontFamily: 'Baskerville',
              color: Colors.brown,
              fontSize: widget._mealFontSize,
            ),
          ),
        ),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 20.0),
          child: Divider(
            color: Colors.grey[400],
          ),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return displayLine();
  }
}
