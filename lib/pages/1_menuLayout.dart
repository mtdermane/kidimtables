import 'package:flutter/material.dart';
import '1_menuWidgets/layoutMaster.dart';

///toolbar widget

class TypeListPage extends StatefulWidget {
  final String ? _placeId;
  final String ? _table;
  TypeListPage(this._placeId, this._table);

  @override
  _TypeListPageState createState() => _TypeListPageState();
}

class _TypeListPageState extends State<TypeListPage> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return DefaultLayout('Menu', widget._placeId, this.widget._table);
  }
}