import 'package:flutter/material.dart';
import '../../widgets/globalVariables.dart' as global;
import '../../models/cartContent.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:http/http.dart';
import 'dart:async';
import '../../models/posts.dart';
import '../../translations.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';

///Content of the page that send the order
class SendOrderContent extends StatefulWidget {
  static final _orderImagePath = 'images/icon_order.svg';
  final double _orderFontSize = 25.0;
  final double _orderMessageMargin = 20.0;
  final double _buttonWidth = 200.0;
  final double _orderImageSize = 250.0;
  final double _decorativeMargin = 20.0;
  final double _snackBarFontSize = 20.0;
  final int _snackBarDuration = 2;

  final Widget _orderImage = new SvgPicture.asset(
    _orderImagePath,
    color: Colors.green[800],
    semanticsLabel: 'Place order',
  );

  static final _decorativeImage = 'images/alter_decorative_line.svg';

  final Widget _decorativeLine = new SvgPicture.asset(
    _decorativeImage,
    semanticsLabel: 'Decorative line',
  );

  final String ? _table;
  final String ? _placeId;


  SendOrderContent(this._table, this._placeId);

  @override
  _SendOrderContentState createState() => _SendOrderContentState();
}

class _SendOrderContentState extends State<SendOrderContent> {
  bool ? _showAnimation;

  @override
  void initState() {
    super.initState();
    _showAnimation = true;
  }

  ///function that send the order
  ///Function used to create a post
  Future<bool> createPost(String url, {Map ? body}) async {
    return post(url as Uri, body: body).then((Response response) {
      final int statusCode = response.statusCode;
      return statusCode == 201;
    });
  }

  void sendOrder(BuildContext context) async {

    //getting the url
    String url;
    String ? hostname = await global.storage.read(key: "serverAddress");
    url = "${hostname}orders";

    //creating the order object
    List<CartContent> order = global.cart;
    PostOrderModel postOrderModel = new PostOrderModel(placeId: widget._placeId, table: widget._table, order: order);

    //sending the order object
    try {
      bool orderSent = await createPost(url, body: postOrderModel.toMap());
      if (orderSent != null) {
        if (orderSent) {
          global.cart.clear();
          EasyLoading.showSuccess(Translations.of(context)!.text('done'));
          Navigator.pop(context);
        } else {
          EasyLoading.showError(Translations.of(context)!.text('sendError'));
        }
      }
    }
    catch (e) {
      EasyLoading.showError(Translations.of(context)!.text('sendError'));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Stack(
        children: <Widget>[
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                width: widget._orderImageSize,
                height: widget._orderImageSize,
                child: widget._orderImage,
              ),
              Container(
                margin: EdgeInsets.symmetric(
                  vertical: 0.0,
                  horizontal: widget._orderMessageMargin  ,
                ),
                child: Text(
                  Translations.of(context)!.text('orderText'),
                  maxLines: 5,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontFamily: 'Baskerville',
                    fontSize: widget._orderFontSize,
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(
                    horizontal: widget._decorativeMargin, vertical: 30.0),
                child: widget._decorativeLine,
              ),
              Container(
                width: widget._buttonWidth,
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    backgroundColor: Colors.red.shade600
                  ),
                  child: Text(
                    Translations.of(context)!.text('send'),
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontFamily: 'Baskerville',
                      color: Colors.white,
                      fontSize: widget._orderFontSize,
                    ),
                  ),
                  onPressed: () {
                    EasyLoading.show(status: Translations.of(context)!.text('sending'));
                    sendOrder(context);
                  },
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}