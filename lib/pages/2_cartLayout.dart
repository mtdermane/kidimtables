import 'package:flutter/material.dart';
import '../widgets/globalVariables.dart' as global;
import '../models/cartContent.dart' as cartItemContent;
import '../widgets/defaultLayout.dart';
import '2_cartWidgets/cartContent.dart';
import '2_cartWidgets/emptyCard.dart';
import '../translations.dart';

///Cart page
class CartPage extends StatefulWidget {
  @override
  _CartPageState createState() => _CartPageState();
}

class _CartPageState extends State<CartPage> {
  List<cartItemContent.CartContent> _cart = global.cart;

  updateLayout(){
    setState(() {
      _cart = global.cart;
    });
  }

  Widget cartClassToDisplay(){
    return global.cart.length > 0 ? CartContent(updateLayout) : CartEmpty();
  }

  @override
  Widget build(BuildContext context) {
    return CartLessLayout(Translations.of(context)!.text('cart'), cartClassToDisplay());
  }
}