import 'package:flutter/material.dart';
import 'pages/0_loginLayout.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'translations.dart';

import 'widgets/globalVariables.dart' as global;

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  final String serverAddress = 'https://kidim-backend.appspot.com/';
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

  Future<void> _addSecureItems(String key, value) async {
    await global.storage.write(key: key, value: value);
  }

  @override
  void initState() {
    super.initState();

  }
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {

    _addSecureItems('serverAddress', widget.serverAddress);
    return FlutterEasyLoading(
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Kidim',
        theme: ThemeData(
          primarySwatch: Colors.brown,
        ),
        localizationsDelegates: [
          const TranslationsDelegate(),
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
        ],
        supportedLocales: [
          const Locale('en', ''),
          const Locale('fr', ''),
        ],
        home: Scaffold(
          backgroundColor: Colors.orange[50],
          body: SafeArea(child: LoginPage(),),
        ),
      ),
    );
  }
}
