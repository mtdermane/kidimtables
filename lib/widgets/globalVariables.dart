library kidim.globals;

import 'dart:collection';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import '../models/cartContent.dart';


//Storage of the critical information
final storage = new FlutterSecureStorage(); //critical data storage

//the downloading menu will be store here
Map menu = new HashMap(); //menu loaded storage

//Cart content will be listed here
List<CartContent> cart = <CartContent>[];

String language = "en";