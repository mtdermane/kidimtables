import 'package:flutter/material.dart';

class CartLessLayout extends StatefulWidget {
  //props required to use the default layout title is the tile of toolbar
  final String _title;
  //content is the body content
  final Widget _content;

  CartLessLayout(this._title, this._content);

  @override
  _CartLessLayoutState createState() => _CartLessLayoutState();
}

class _CartLessLayoutState extends State<CartLessLayout> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.orange[50],
      appBar: AppBar(
        title: Text(
          widget._title,
          style: TextStyle(color: Colors.white),
        ),
        backgroundColor: Colors.red[800],
      ),
      body: widget._content,
    );
  }
}
