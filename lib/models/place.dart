class Place {
  String _id;
  String _name;


  Place(this._id, this._name);

  String get name => _name;

  String get id => _id;
}