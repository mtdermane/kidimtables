import 'dart:collection';

class Option {
  String _name;
  String _price;

  Option(this._name, this._price);

  String get price => _price;

  String get name => _name;

  Map jsonVersion() {
    Map result = new HashMap();
    result.putIfAbsent('name', ()=>_name);
    result.putIfAbsent('price', ()=>_price);
    return result;
  }

}