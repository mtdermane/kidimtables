import 'dart:convert';
import 'option.dart';

class CartContent{
  final String name;
  final String price;
  final Option? option;
  final List<String>? addons;


  CartContent({required this.name, required this.price, this.option, this.addons});

  factory CartContent.fromJson(Map<String, dynamic> json) {
    return CartContent(
      name: json['qrCode'],
      price: json['googleId'],
      option: json['option'],
      addons: json['addons'],
    );
  }

  Map toMap() {
    var map = new Map<String, dynamic>();
    map["name"] = name;
    map["price"] = price;
    map["option"] = jsonEncode(option?.jsonVersion() ?? "");

    List addonsMapped = [];
    for(int i = 0; i < addons!.length; i++){
      var anAddonMap = new Map<String, dynamic>();
      anAddonMap["name"] = addons![i];
      addonsMapped.add(anAddonMap);
    }

    map["addons"] = jsonEncode(addonsMapped);
    return map;
  }
}
