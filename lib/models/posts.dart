import 'dart:convert';
import 'cartContent.dart';

///Post class to send requests to get menu
class PostModel {
  final String ? placeId;
  final String ? language;

  PostModel({this.placeId, this.language});

  factory PostModel.fromJson(Map<String, dynamic> json) {
    return PostModel(
        placeId: json['placeId'],
      language: json['language'],
    );
  }

  Map toMap() {
    var map = new Map<String, dynamic>();
    map["placeId"] = placeId;
    map["language"] = language;
    return map;
  }
}

///Post class to send orders
class PostOrderModel {
  final String ? placeId;
  final String ? table;
  final List<CartContent> ? order;

  PostOrderModel({this.placeId, this.table, this.order});

  factory PostOrderModel.fromJson(Map<String, dynamic> json) {
    return PostOrderModel(
        placeId: json['placeId'], table: json['table'], order: json['order']
    );
  }

  Map toMap() {
    var map = new Map<String, dynamic>();
    map["placeId"] = placeId;
    map["table"] = table;
    List orderMapped = [];
    for(int i = 0; i < order!.length; i++){
      orderMapped.add(order![i].toMap());
    }
    map["order"] = jsonEncode(orderMapped);

    return map;
  }
}

///Post class to send orders
class PostLogin {
  final String ? username;
  final String ? password;
  final String ? strategy;

  PostLogin({this.username, this.password, this.strategy});

  factory PostLogin.fromJson(Map<String, dynamic> json) {
    return PostLogin(
        username: json['username'], password: json['password'], strategy: json['strategy']
    );
  }

  Map toMap() {
    var map = new Map<String, dynamic>();
    map["username"] = username;
    map["password"] = password;
    map["strategy"] = strategy;
    return map;
  }
}