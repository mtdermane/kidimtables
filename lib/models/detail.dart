import 'option.dart';
import 'extras.dart';
class Detail {
  late String _name;
  late double _price;
  String _description;
  List<Option> _option;
  List<Extras> _addon;

  Detail(this._name, this._price, this._description, this._option, this._addon);

  List<Extras> get addon => _addon;

  List<Option> get option => _option;

  String get description => _description;

  double get price => _price;

  String get name => _name;
}