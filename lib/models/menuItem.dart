class MenuItem{
  String _name;
  String _price;
  String _description;
  List _options;

  MenuItem(this._name, this._price, this._description, this._options);

  String get description => _description;

  String get price => _price;

  String get name => _name;

  List get options => _options;

  @override
  bool operator ==(Object other) => other is MenuItem && other.name == _name;

  @override
  int get hashCode => _name.hashCode;

}

class MenuList{

  String _category;
  List _items;


  MenuList(this._category, this._items);

  String get category   => _category;

  List get items => _items;
}
