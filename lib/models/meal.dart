class Meal {
  String _name;
  String _price;

  Meal(this._name, this._price);

  String get price => _price;

  String get name => _name;

}