class CartItem{

  String _meal;
  String _price;
  String _option;

  List<String> _addons;

  CartItem(this._meal, this._price, this._option, this._addons);

  List<String> get addons => _addons;

  String get option => _option;

  String get price => _price;

  String get meal => _meal;

}